//redirect when button is clicked
function redirectTo(url) {
	window.location.href = url;
}

//run when all assets on website is ready
$(window).on('load', function(){
	//check if ran on inapp browser
	var userAgent = window.navigator.userAgent.toLowerCase(),
		inapp = /instagram|fbav|fban|kakao|snapchat|line|micromessenger|twitter|wechat/.test(userAgent),
		ios = /iphone|ipod|ipad/.test(userAgent),
		safari = /safari/.test(userAgent),
		android = /android/.test(userAgent),
		chrome = /chrome/.test(userAgent),
		location = window.location.href;

	//clipboard actions
	var clipboard = new ClipboardJS('.btn');
	
	clipboard.on('success', function(e) {
		alert("Copied to clipboard!")
	});
	
	clipboard.on('error', function(e) {
		alert("Copy to clipboard error!")
	});

	//if inapp browser is detected then the user will be prompted
	if(inapp){
		$('.prompt-container').show();
		$('.content, a-scene').remove();
		$('#copy-input-link').val(location);
		if (android) {
			$(".browser-logo").attr("src","assets/img/chrome.png");
			$('p').each(function() {
				var text = $(this).text();
				text = text.replace('Safari', 'Chrome').replace('iOS', 'Android'); 
				$(this).text(text);
			});
		}
	} else {
		//if it's detected on ios, but not using safari, then we will open safari
		if(ios && !safari) {
			window.open('x-web-search://?ar', "_self");
			window.location.href = location;
		} else if (android && !chrome) {
			//if it's on android, but not using chrome then we will open chrome
			window.location.href = 'googlechrome://navigate?url='+ location;
		} else {
			//Fetch camera permission for AR/AFRAME
			var facingCamera = "user";
			if(ios || android){
				facingCamera = "environment";
			}

			var constraints = { 
				video: {
					advanced: [
						{
							facingMode: facingCamera
						}
					]
				} 
			};

			var errorCallback = function(error) {
				if (error.name == 'NotAllowedError') {
					alert("Please enable camera for AR experience.\r\nSetting > Browser name > Allow camera")
				}
			};
			navigator.mediaDevices.getUserMedia(constraints)
				.then(null, errorCallback);


			AFRAME.registerComponent("foo",{
				init:function() {
					//action on pinch and pan on the model to rotate and scale accordingly'
					var element = document.querySelector('body');
					var model = document.querySelector('#animated-model');
					var hammertime = new Hammer(element);
					var pinch = new Hammer.Pinch(); // Pinch is not by default in the recognisers
					hammertime.add(pinch); // add it to the Manager instance
					
					//hide loader when model is loaded
					model.addEventListener('model-loaded', e => {
						$(".arjs-loader").hide();
					})
			
					//rotate model on pan
					hammertime.on('pan', (ev) => {
						let rotation = model.getAttribute("rotation")
						switch(ev.direction) {
							case 2:
								rotation.y = rotation.y + 4
								break;
							case 4:
								rotation.y = rotation.y - 4
								break;
							case 8:
								rotation.x = rotation.x + 4
								break;
							case 16:
								rotation.x = rotation.x - 4
								break;
							default:
								break;
						}
						model.setAttribute("rotation", rotation)
					});
			
					//scale => zoom and shrink model on pinch
					const initialScale = model.getAttribute("scale").x;
					const maxScale = initialScale*2; 
			
						hammertime.on("pinch", (ev) => {
						let pinch = ev.scale;
						let scale = model.getAttribute("scale").x*pinch;
						//limit zoom until 2times of initial size and shrink until initial scale
						if(scale <= maxScale && scale >= initialScale){
							let newScale = {x:scale, y:scale, z:scale}
							model.setAttribute("scale", newScale);
						}
					});
				}
			});				
		}
	}

	//display socmed buttons when burger button is clicked
	var burger = $(".burger");
	var displayBurgerClicked = $(".displayBurgerClicked");
	var isBurgerClicked = false;
	
	burger.click(function() {
		if(!isBurgerClicked){
			isBurgerClicked = true;
			displayBurgerClicked.css("display", "grid");
		} else {
			isBurgerClicked = false;
			displayBurgerClicked.hide();
		}
	});
});
